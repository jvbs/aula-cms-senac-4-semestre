<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'aula_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin@123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'TKm9Q>V6Tru}XJP7lP6Y89},^?b)+J`t.KU0BSY;qqUZZX;]!f&cA<fN.Tgf8HyL' );
define( 'SECURE_AUTH_KEY',  'Hpj_DP{.X@/U-VxH),j:; )tp(?|b[*q!RVG&=@Ig6;2theJBe@pF-k;f>0w`6TU' );
define( 'LOGGED_IN_KEY',    '/gSOdqpK+ d{jpcLfd{h.^}CAlx74`Flu_q>cw_@>Q(6)8YNZapks;{mAgoKb}W-' );
define( 'NONCE_KEY',        'q3Elkp7;0Qr[3[<8!z*?r?k(j9ik0!9c:ulQrc@1LrCb]V&Auj}=SRdcnz]Rqs3a' );
define( 'AUTH_SALT',        'LbI:SNXd|=UpU(tSjjV8}CB- XJ=%u}O]:}{~xrutVT}%E)T4K|RN_5>i;uZfHOe' );
define( 'SECURE_AUTH_SALT', '?Z+wwgj;5O-^7+K {=N~?OGC}Jt,7aJ(5;/3Nc LTxM0G?g k1]HoRAaPF2+L0>8' );
define( 'LOGGED_IN_SALT',   '`H-4bW,S4vrUU_ 7ht@W`xnAk6>9I}O]&>|x2bVJFcn)]s;l%.LiR=VG)l:-2bez' );
define( 'NONCE_SALT',       'y>$_n>aE`BW%!C~2u];0I}w3}gAjDw+(_7Iy?e,#iY3FO#R.aaW{L51?W-UPh9:W' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
